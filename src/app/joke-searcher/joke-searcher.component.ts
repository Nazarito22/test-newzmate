import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {JokeService} from "@app/joke-searcher/jokes.service";

@Component({
  selector: 'jokes-searcher',
  templateUrl: './joke-searcher.component.html',
  styleUrls: ['./joke-searcher.component.scss']
})
export class JokeSearcherComponent implements OnInit {

  findJokeForm: FormGroup;
  jokes: Array<any> = [];

  constructor(private formBuilder: FormBuilder,
              public jokeService: JokeService) {
    this.createForm();
  }

  ngOnInit() {
  }

  findJoke() {
    this.jokeService.getJokes(this.findJokeForm.value.joke).subscribe(
      response => {
        this.jokes = response.result;
      },
      error => {
        console.log(error);
      })
  }

  private createForm() {
    this.findJokeForm = this.formBuilder.group({
      joke: ['', Validators.required]
    });
  }

}
