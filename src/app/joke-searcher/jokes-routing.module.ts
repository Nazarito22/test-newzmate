import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route, extract } from '@app/core';
import {JokeSearcherComponent} from "@app/joke-searcher/joke-searcher.component";
const routes: Routes = [
  Route.withShell([
    { path: 'jokes-searcher', component: JokeSearcherComponent, data: { title: extract('Jokes') } }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})

export class JokesRoutingModule { }
