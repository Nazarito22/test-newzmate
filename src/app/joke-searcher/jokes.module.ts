import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {JokesRoutingModule} from "@app/joke-searcher/jokes-routing.module";
import {JokeSearcherComponent} from '@app/joke-searcher/joke-searcher.component';
import {ReactiveFormsModule} from "@angular/forms";
import {JokeService} from "@app/joke-searcher/jokes.service";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    JokesRoutingModule
  ],
  declarations: [
    JokeSearcherComponent
  ],
  providers: [
    JokeService
  ]
})
export class JokesModule { }
